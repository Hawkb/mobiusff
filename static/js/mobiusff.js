angular.module('mobiusFF', [])
    .controller('CardController', function($scope, $http, $log){
            $log.log("Getting cards");
            $http.get('cards')
                .success(function(data, status, headers, config){
                    $scope.Cards = data;
                });
    });