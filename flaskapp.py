from flask import Flask, render_template, jsonify, url_for
from data.cards import cards

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


# API
@app.route('/cards/')
def get_all_cards():
    return jsonify([vars(card) for card in cards])


@app.route('/cards/<number>')
def get_card_by_id(number):
    found_card = next(card for card in cards if card.number == number)
    return jsonify(vars(found_card))


if __name__ == "__main__":
    app.run()
